ARG APP=cryptonic
ARG BUILD_CONFIGURATION=production
ARG PORT=80

FROM nginx:1.18.0-alpine AS base-spa
ARG PORT
EXPOSE ${PORT}

COPY .ci/nginx/default.conf /etc/nginx/conf.d/default.conf

FROM node:14.16.1-alpine AS build
ARG APP
ARG BUILD_CONFIGURATION
WORKDIR /app
COPY package.json yarn.lock* ./
RUN yarn install:ci
COPY . .
RUN yarn build ${APP} --configuration=${BUILD_CONFIGURATION} --verbose

FROM base-spa
ARG APP
COPY --from=build /app/dist/apps/${APP} /usr/share/nginx/html
