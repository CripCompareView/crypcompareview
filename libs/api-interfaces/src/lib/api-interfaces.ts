export interface CryptoCurrencies {
  key: string;
  name: string;
  price: string;
}

export interface ApiResponse {
  currencies: CryptoCurrencies[];
}
