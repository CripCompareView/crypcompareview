import { Component } from '@angular/core';

@Component({
  selector: 'crypto-compare-view-root',
  template: `<router-outlet></router-outlet>`,
})
export class AppComponent {}
