import { Module } from '@nestjs/common';
import { ApiConfigModule } from './api-config';
import { CryptoModule } from './crypto/crypto.module';
import { HealthModule } from './health';

@Module({
  imports: [ApiConfigModule, HealthModule, CryptoModule],
  providers: [],
})
export class AppModule {}
