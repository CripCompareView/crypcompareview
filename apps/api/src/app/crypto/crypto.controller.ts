import { ApiResponse } from '@crypto-compare-view/api-interfaces';
import { Controller, Get, Param } from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { CryptoService } from './crypto.service';
import { CryptoCurrenciesResponse } from './dtos/crypto-currencies-response.dto';
import { CryptoCurrenciesParam } from './dtos/currency.parm';

@Controller('crypto')
@ApiTags('Crypto')
export class CryptoController {
  constructor(private readonly service: CryptoService) {}

  @Get(':currency')
  @ApiOkResponse({ type: CryptoCurrenciesResponse })
  // TODO: add some swagger decoretors for all possible errors
  async getCryptoCurrencies(
    @Param() param: CryptoCurrenciesParam
  ): Promise<CryptoCurrenciesResponse> {
    const currency = param.currency;
    const result = await this.service.getAllCurrencies(currency);

    return {
      currencies: Object.keys(result).map((key) => ({
        key,
        name: mapCryptoName(key),
        price: formatPrice(result[key][currency], currency),
      })),
    } as CryptoCurrenciesResponse;
  }
}

const mapCryptoName = (key: string): string => {
  switch (key) {
    case 'BTC':
      return 'Bitcoin';
    case 'ETH':
      return 'Ethereum';
    case 'XRP':
      return 'Ripple';
    case 'LTC':
      return 'Litecoin';
    case 'BCH':
      return 'Bitcoin Cash';
    case 'ETC':
      return 'Ethereum Classic';

    default:
      return '';
  }
};

const formatPrice = (price: number, currency: string): string => {
  return `${currency} ${price}`;
};
