import {
  ApiResponse,
  CryptoCurrencies,
} from '@crypto-compare-view/api-interfaces';
import { ApiProperty } from '@nestjs/swagger';

export class CryptoCurrenciesItem implements CryptoCurrencies {
  @ApiProperty({
    example: 'BTC',
    type: String,
  })
  key: string;

  @ApiProperty({
    example: 'Bitcoin',
    type: String,
  })
  name: string;

  @ApiProperty({
    example: '$ 44833.34',
    type: String,
  })
  price: string;
}

export class CryptoCurrenciesResponse implements ApiResponse {
  @ApiProperty({
    example: [
      {
        key: 'BTC',
        name: 'Bitcoin',
        price: 'EUR 36874.81',
      },
      {
        key: 'ETH',
        name: 'Ethereum',
        price: 'EUR 2786.27',
      },
      {
        key: 'XRP',
        name: 'Ripple',
        price: 'EUR 1.25',
      },
      {
        key: 'LTC',
        name: 'Litecoin',
        price: 'EUR 249.86',
      },
      {
        key: 'BCH',
        name: 'Bitcoin Cash',
        price: 'EUR 921.17',
      },
      {
        key: 'ETC',
        name: 'Ethereum Classic',
        price: 'EUR 75.88',
      },
    ],
    type: String,
  })
  currencies: CryptoCurrenciesItem[];
}
