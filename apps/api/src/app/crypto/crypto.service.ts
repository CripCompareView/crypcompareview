import { HttpService, Injectable, Logger } from '@nestjs/common';
import { from, Observable } from 'rxjs';
import { ApiConfigService } from '../api-config';

@Injectable()
export class CryptoService {
  private logger: Logger;

  constructor(
    private readonly configService: ApiConfigService,
    private readonly httpService: HttpService
  ) {}

  async getAllCurrencies(currency: string) {
    const url = this.configService.apiUrl;
    const apiKy = this.configService.apiKey;
    const fsyms = 'BTC,ETH,XRP,LTC,BCH,ETC';
    const uri = `${url}/data/pricemulti?api_key=${apiKy}&fsyms=${fsyms}&tsyms=${currency}`;

    const resp = await from(this.httpService.get(uri)).toPromise();

    return resp.data;
  }
}
